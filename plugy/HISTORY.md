# Release History
This is a summary of the changelog.

## 0.1.0:
* Reading fluorescence acquisition sequences
* Cleaning data with various filters and signal processing methods
* Detection of plugs, barcodes and samples

## 0.2.0:
* Logging and session handling
